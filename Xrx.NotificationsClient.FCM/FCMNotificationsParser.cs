﻿using System.Collections.Generic;
using Xrx.NotificationsClient.Abstractions;

namespace Xrx.NotificationsClient.FCM
{
    public class FCMNotificationsParser : INotificationsParser
    {
        public INotification Parse(IDictionary<string, object> rawMessage)
        {
            INotification notification = new Notification
            {
                Title = rawMessage["title"].ToString(),
                Body = rawMessage["body"].ToString(),
                Badge = rawMessage["badge"].ToString()
            };
            return notification;
        }
    }
}
