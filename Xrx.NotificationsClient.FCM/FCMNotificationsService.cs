﻿using Plugin.FirebasePushNotification;
using Plugin.FirebasePushNotification.Abstractions;
using Xrx.NotificationsClient.Abstractions;

namespace Xrx.NotificationsClient.FCM
{
    public class FCMNotificationsService : AbstractNotificationsService
    {
        protected INotificationsParser notificationsParser;
        public FCMNotificationsService()
        {
            notificationsParser = new FCMNotificationsParser();
        }
        public override void Initialize()
        {
            CrossFirebasePushNotification.Current.Subscribe("general");
            CrossFirebasePushNotification.Current.OnTokenRefresh += TokenRefreshed;
            CrossFirebasePushNotification.Current.OnNotificationReceived += NotificationReceived;
            CrossFirebasePushNotification.Current.OnNotificationOpened += NotificationOpened;
            if(!string.IsNullOrEmpty(CrossFirebasePushNotification.Current.Token))
            {
                NotifyObservers(NotificationEvent.DeviceTokenUpdated, CrossFirebasePushNotification.Current.Token);
            }
        }
        protected virtual void TokenRefreshed(object sender, FirebasePushNotificationTokenEventArgs e)
        {
            NotifyObservers(NotificationEvent.DeviceTokenUpdated, e.Token);
        }

        protected virtual void NotificationOpened(object sender, FirebasePushNotificationResponseEventArgs e)
        {
            ActiveNotification = notificationsParser.Parse(e.Data);
            NotifyObservers(NotificationEvent.Opened, ActiveNotification);
        }
        protected virtual void NotificationReceived(object sender, FirebasePushNotificationDataEventArgs e)
        {
            ActiveNotification = notificationsParser.Parse(e.Data);
            NotifyObservers(NotificationEvent.Received, ActiveNotification);
        }

        public override void ActiveNotificationHandled()
        {
            ActiveNotification = null;
        }
    }
}
