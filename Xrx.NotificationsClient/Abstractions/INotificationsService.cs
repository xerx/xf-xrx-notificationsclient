﻿namespace Xrx.NotificationsClient.Abstractions
{
    public interface INotificationsService
    {
        void Initialize();
        void Register(INotificationsObserver observer);
        void Unregister(INotificationsObserver observer);
        void NotifyObservers(NotificationEvent @event, object data);
        void ActiveNotificationHandled();
        INotification ActiveNotification { get; }
    }
}
