﻿using System.Collections.Generic;

namespace Xrx.NotificationsClient.Abstractions
{
    public interface INotificationsParser
    {
        INotification Parse(IDictionary<string, object> data);
    }
}
