﻿namespace Xrx.NotificationsClient.Abstractions
{
    public interface INotification
    {
        string Title { get; set; }
        string Body { get; set; }
        string Badge { get; set; }
    }
}
