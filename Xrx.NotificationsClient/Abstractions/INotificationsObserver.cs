﻿namespace Xrx.NotificationsClient.Abstractions
{
    public interface INotificationsObserver
    {
        void Notify(NotificationEvent @event, object data);
    }
}
