﻿using System.Collections.Generic;

namespace Xrx.NotificationsClient.Abstractions
{
    public abstract class AbstractNotificationsService : INotificationsService
    {
        protected HashSet<INotificationsObserver> observers;
        public AbstractNotificationsService()
        {
            observers = new HashSet<INotificationsObserver>();
        }

        public INotification ActiveNotification { get; protected set; }

        public abstract void ActiveNotificationHandled();

        public abstract void Initialize();

        public virtual void NotifyObservers(NotificationEvent @event, object data)
        {
            foreach (var observer in observers)
            {
                observer?.Notify(@event, data);
            }
        }

        public virtual void Register(INotificationsObserver observer)
        {
            if(!observers.Contains(observer))
            {
                observers.Add(observer);
            }
        }

        public virtual void Unregister(INotificationsObserver observer)
        {
            if (observers.Contains(observer))
            {
                observers.Remove(observer);
            }
        }
    }
}
