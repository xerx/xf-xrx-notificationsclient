﻿using Xrx.NotificationsClient.Abstractions;

namespace Xrx.NotificationsClient
{
    public class Notification : INotification
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string Badge { get; set; }
    }
}
