﻿namespace Xrx.NotificationsClient
{
    public enum NotificationEvent
    {
        Received, Opened, DeviceTokenUpdated
    }
}
